import 'package:blist/core/network/dio/dio_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:blist/features/auth/data/auth_datasource.dart';

// Mock class for DioService
class MockDioService extends Mock implements DioService {}

void main() {
  late MockDioService mockDioService;
  late AuthDataSource authDataSource;

  // Initialize the mock DioService and AuthDataSource before each test
  setUp(() {
    mockDioService = MockDioService();
    authDataSource = AuthDataSource(mockDioService);
  });

  test('should return data when login is called', () async {
    // Arrange:
    final Map<String, dynamic> mockResponse = {
      "id": 1,
      "username": "atuny0",
      "email": "atuny0@sohu.com",
      "firstName": "Terry",
      "lastName": "Medhurst",
      "gender": "male",
      "image": "https://robohash.org/Terry.png?set=set4",
      "token": "String"
    };
    //This sets up a condition for the mock object mockDioService for example using the post method of the dio service
    when(
      authDataSource.login(
        {
          'username': 'atuny0',
          'password': '9uQFF1Lh',
        },
      ),
    ).thenAnswer(
      (_) async {
        return mockResponse;
      },
    );
    // Act: Call the login method
    final result = await authDataSource.login({
      'username': 'atuny0',
      'password': '9uQFF1Lh',
    });

    // Assert: Verify the result and that the method was called once
    expect(result, mockResponse);
    verify(authDataSource.login).called(1);
  });
}
