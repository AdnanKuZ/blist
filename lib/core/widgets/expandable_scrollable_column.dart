import 'package:flutter/material.dart';

/// This widget allows usage of widgets like [Expanded] and [Spacer] inside a scrollable column
/// However, it uses [IntrinsicHeight] which is very expensive, meaning this widget should only be used when necessary
class ExpandableScrollableColumn extends StatelessWidget {
  final EdgeInsets padding;
  final List<Widget> children;
  final ScrollPhysics? physics;
  final ScrollController? controller;
  final CrossAxisAlignment crossAxisAlignment;

  const ExpandableScrollableColumn({
    required this.children,
    this.padding = EdgeInsets.zero,
    this.physics,
    this.controller,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return SingleChildScrollView(
        physics: physics,
        padding: padding,
        controller: controller,
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: constraints.maxHeight - padding.vertical,
          ),
          child: IntrinsicHeight(
            child: Column(
              crossAxisAlignment: crossAxisAlignment,
              children: children,
            ),
          ),
        ),
      );
    });
  }
}
