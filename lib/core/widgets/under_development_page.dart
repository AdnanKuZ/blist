import 'package:flutter/material.dart';
import 'package:blist/gen/assets.gen.dart';
import 'package:lottie/lottie.dart';

// Remember to remove this widget from anywhere in final versions
class UnderDevelopmentPlaceholder extends StatelessWidget {
  const UnderDevelopmentPlaceholder({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Lottie.asset(Assets.json.underDevelopment),
        const Text(
          'Page Under Construction',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
        ),
        const SizedBox(height: 80),
      ],
    );
  }
}
