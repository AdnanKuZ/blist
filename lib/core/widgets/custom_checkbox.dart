import 'package:blist/app/theme/styles/colors.dart';
import 'package:flutter/material.dart';

class MyCustomCheckbox extends StatelessWidget {
  final bool value;
  final bool loading;
  final ValueChanged<bool?> onChanged;
  final Color borderColor;
  final Duration animationDuration;

  const MyCustomCheckbox({
    super.key,
    required this.value,
    this.loading = false,
    required this.onChanged,
    required this.borderColor,
    this.animationDuration = const Duration(milliseconds: 200),
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onChanged(!value);
      },
      child: AnimatedContainer(
        duration: animationDuration,
        decoration: BoxDecoration(
          border: Border.all(
            color: !value ? borderColor : Colors.transparent,
            width: 1.2,
          ),
          shape: BoxShape.circle,
          color: loading
              ? AppColors.violet.withOpacity(.5)
              : value
                  ? AppColors.violet
                  : Colors.transparent,
        ),
        width: 22.0,
        height: 22.0,
        child: value
            ? Center(
                child: AnimatedCrossFade(
                  firstChild: const Icon(
                    Icons.check,
                    color: Colors.transparent,
                    size: 16.0,
                  ),
                  secondChild: const Icon(
                    Icons.check,
                    color: AppColors.primary,
                    size: 16.0,
                  ),
                  crossFadeState: CrossFadeState.showSecond,
                  duration: animationDuration,
                ),
              )
            : null,
      ),
    );
  }
}
