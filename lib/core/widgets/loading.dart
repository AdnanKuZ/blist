import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:blist/gen/assets.gen.dart';
import 'package:lottie/lottie.dart';

/// A widget that applies a blur effect over its [child] widget.
/// It also provides an [onTap] callback that can be used to trigger actions when the widget is tapped.
class BlurContainer extends StatelessWidget {
  final Widget? child;
  final VoidCallback? onTap;

  const BlurContainer({
    this.child,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 4,
          sigmaY: 4,
        ),
        child: Container(
          alignment: Alignment.center,
          child: child,
        ),
      ),
    );
  }
}

/// A widget that shows a loading overlay with a [BlurContainer].
class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay({super.key});

  @override
  Widget build(BuildContext context) {
    return BlurContainer(
      child: Lottie.asset(Assets.json.loading, width: 200),
    );
  }
}
