import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:blist/app/theme/styles/colors.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    super.key,
    this.isObscur,
    this.onFieldSubmit,
    this.onSaved,
    this.suffix,
    this.prefix,
    this.focusedBorder,
    this.textInputAction = TextInputAction.done,
    this.validator,
    this.fillColor = Colors.white,
    this.textColor = Colors.black,
    this.hintColor = AppColors.hint,
    this.controller,
    this.fontSize = 14,
    this.keyboardType = TextInputType.emailAddress,
    this.padding,
    this.hintText = '',
    this.textDirection = TextDirection.ltr,
    this.borderRadius,
    this.inputFormatters,
    this.border,
    this.enabledBorder,
    this.disabledBorder,
    this.errorBorder,
    this.textStyle,
    this.hintStyle,
    this.borderSide,
    this.disabledBorderSide,
    this.focusedBorderSide,
    this.enabledBorderSide,
    this.errorBorderSide,
    this.focusNode,
    this.maxLines,
    this.labelText,
    this.onChanged,
  });

  final void Function(String?)? onSaved;
  final String? Function(String?)? validator;
  final void Function(String?)? onFieldSubmit;
  final TextInputAction textInputAction;
  final bool? isObscur;
  final TextEditingController? controller;
  final Widget? suffix;
  final Widget? prefix;
  final Color fillColor;
  final TextInputType keyboardType;
  final Color textColor;
  final TextStyle? textStyle;
  final Color hintColor;
  final double fontSize;
  final EdgeInsetsGeometry? padding;
  final String hintText;
  final TextDirection textDirection;
  final double? borderRadius;
  final List<TextInputFormatter>? inputFormatters;
  final BorderSide? borderSide;
  final InputBorder? border;
  final BorderSide? enabledBorderSide;
  final InputBorder? enabledBorder;
  final InputBorder? focusedBorder;
  final BorderSide? errorBorderSide;
  final InputBorder? errorBorder;
  final BorderSide? disabledBorderSide;
  final BorderSide? focusedBorderSide;
  final InputBorder? disabledBorder;
  final FocusNode? focusNode;
  final Function(String text)? onChanged;
  final TextStyle? hintStyle;
  final String? labelText;

  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlign: TextAlign.left,
      controller: controller,
      maxLines: maxLines,
      focusNode: focusNode,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
          errorStyle: const TextStyle(height: 0),
          focusedBorder: focusedBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: focusedBorderSide ?? BorderSide.none),
          border: border ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: borderSide ?? BorderSide.none),
          enabledBorder: enabledBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: enabledBorderSide ?? BorderSide.none),
          disabledBorder: disabledBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: disabledBorderSide ?? BorderSide.none),
          errorBorder: errorBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: errorBorderSide ??
                      const BorderSide(
                        color: Color(0xFFF01738),
                      )),
          hintText: hintText,
          contentPadding: padding ??
              const EdgeInsets.symmetric(horizontal: 14, vertical: 8),
          filled: true,
          labelText: labelText,
          alignLabelWithHint: true,
          prefixIcon: prefix,
          fillColor: fillColor,
          suffixIcon: suffix,
          hintTextDirection: TextDirection.rtl,
          hintStyle: hintStyle ??
              TextStyle(
                color: hintColor,
              )
          // prefix: prefix,
          ),
      obscuringCharacter: '*',
      style: textStyle ??
          TextStyle(
              letterSpacing: (isObscur != null && isObscur!) ? 3 : 0,
              color: textColor,
              fontSize: fontSize),
      textDirection: textDirection,
      obscureText: isObscur ?? false,
      keyboardType: keyboardType,
      textInputAction: textInputAction,
      validator: validator,
      onChanged: onChanged,
      onSaved: onSaved,
      onFieldSubmitted: onFieldSubmit,
    );
  }
}
