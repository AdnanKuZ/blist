abstract class Validators {
  static final emailRegex = RegExp(
    r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$',
  );

  static final syrianPhoneRegex = RegExp(
    r'^((\+|00)?9639|0?9)([3-6]|[8,9])\d{7}$',
  );

  static String getTextFieldValidationInfo(
    String email,
    String password, {
    bool isSignUp = false,
    String? userName,
    String? confirmPass,
    String? phone,
  }) {
    String message = '';

    if (email.isEmpty) {
      message = 'Email should Not Be Empty';
    }

    if (!emailRegex.hasMatch(email) && email.isNotEmpty) {
      message = '$message\nInvalid email address format';
    }

    if (password.isEmpty) {
      message = '$message\nPassword should Not Be Empty';
    }

    if (password.length < 6 && password.isNotEmpty) {
      message = '$message\nPassword should not be less than 6 characters';
    }

    if (isSignUp) {
      if (confirmPass != password) {
        message = '$message\nPasswords do not match';
      }

      if (userName!.isEmpty) {
        message = '$message\nUsername should not be empty';
      }

      if (phone!.isEmpty) {
        message = '$message\nPhone should not be empty';
      }

      if (!syrianPhoneRegex.hasMatch(phone) && phone.isNotEmpty) {
        message = '$message\nPlease enter a syrian phone number';
      }
    }
    return message.trim();
  }

  static String? emailValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter your email address.';
    }

    if (!emailRegex.hasMatch(value)) {
      return 'Please enter a valid email address.';
    }

    return null;
  }

  static String? passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter your password.';
    }
    if (value.length < 8) {
      return 'Password must be at least 8 characters long.';
    }
    return null;
  }

  static String? nameValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Name should not be empty';
    }
    return null;
  }
}
