import 'package:flutter/material.dart';

mixin PaginationMixin {
  //defining pagination variables
  final ScrollController scrollController = ScrollController();
  int page = 1;
  int? pageCount;

  void pagination(VoidCallback function) {
    // When scroll reaches the end of the screen extent call function()
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      page++;
      function();
    }
  }
}
