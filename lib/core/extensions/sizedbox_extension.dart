import 'package:flutter/material.dart';

extension Space on num {
  /// Vertical Spacer
  SizedBox v() => SizedBox(height: toDouble());

  /// Horizontal Spacer
  SizedBox h() => SizedBox(width: toDouble());
}
