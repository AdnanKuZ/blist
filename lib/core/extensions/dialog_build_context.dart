import 'package:flutter/material.dart' as material;
import 'package:flutter/widgets.dart';

extension DialogBuildContextExtension on BuildContext {
  Future<T?> showDialog<T>(
    Widget dialog, {
    bool barrierDismissible = true,
  }) {
    return material.showDialog<T>(
      context: this,
      builder: (context) => dialog,
      barrierDismissible: barrierDismissible,
    );
  }
}
