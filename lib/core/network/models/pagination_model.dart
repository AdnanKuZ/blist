class PaginationModel {
  final int currentPage;
  final int perPage;
  final int total;
  final int pageCount;

  PaginationModel({
    required this.currentPage,
    required this.perPage,
    required this.total,
    required this.pageCount,
  });

  factory PaginationModel.fromJson(Map<String, dynamic> json) =>
      PaginationModel(
        currentPage: json['current_page'],
        perPage: json['per_page'],
        total: json['total'],
        pageCount: (json['total'] / json['per_page']).ceil(),
      );

  Map<String, dynamic> toJson() => {
        'current_page': currentPage,
        'per_page': perPage,
        'total': total,
      };
}
