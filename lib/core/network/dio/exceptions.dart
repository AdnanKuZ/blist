import 'package:blist/app/injection/injection.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:blist/core/services/shared_preferences_service.dart';

/// An enum that holds names for our custom exceptions.
enum ExceptionType {
  /// The exception for an expired bearer token.
  unAuthorized,

  /// The exception for a prematurely cancelled request.
  canceledRequest,

  /// The exception for a failed connection attempt.
  connectTimeout,

  /// The exception for failing to send a request.
  sendTimeout,

  /// The exception for failing to receive a response.
  receiveTimeout,

  /// The exception for no internet connectivity.
  noInternet,
  formatIncorrect,
  badRequest,
  notFound,
  authorizationError,
  validationError,

  /// The exception for an unknown type of failure.
  unRecognized,

  /// The exception for an unknown exception from the API.
  api,

  /// The exception for any parsing failure encountered during
  /// serialization/deserialization of a request.
  serialization,
}

/// CustomException is used for handling and categorizing exceptions that may occur
/// during network requests and data processing.
class CustomException implements Exception {
  final String name; // Exception name
  final String message; // Exception message
  final Response<dynamic>? errorResponse; // Exception message
  final String? code; // Exception code
  final int? statusCode; // HTTP status code
  final ExceptionType exceptionType; // Type of exception

  /// Constructor for creating a CustomException.
  ///
  /// - [code]: The error code (if available).
  /// - [statusCode]: The HTTP status code (if available).
  /// - [message]: The error message.
  /// - [exceptionType]: The type of exception.
  CustomException({
    this.code,
    int? statusCode,
    required this.message,
    this.errorResponse,
    this.exceptionType = ExceptionType.api,
  })  : statusCode = statusCode ?? 500,
        name = exceptionType.name;

  /// Factory constructor for creating a CustomException from a DioException.
  factory CustomException.fromDioException(Exception error) {
    try {
      if (error is DioException) {
        switch (error.type) {
          case DioExceptionType.cancel:
            return CustomException(
              exceptionType: ExceptionType.canceledRequest,
              statusCode: error.response?.statusCode,
              errorResponse: error.response,
              message: 'Request cancelled prematurely',
            );
          case DioExceptionType.connectionTimeout:
            return CustomException(
              exceptionType: ExceptionType.connectTimeout,
              statusCode: error.response?.statusCode,
              errorResponse: error.response,
              message: 'Connection not established',
            );
          case DioExceptionType.sendTimeout:
            return CustomException(
              exceptionType: ExceptionType.sendTimeout,
              statusCode: error.response?.statusCode,
              errorResponse: error.response,
              message: 'Failed to send',
            );

          case DioExceptionType.receiveTimeout:
            return CustomException(
              exceptionType: ExceptionType.receiveTimeout,
              statusCode: error.response?.statusCode,
              errorResponse: error.response,
              message: 'Failed to receive',
            );

          case DioExceptionType.badResponse:
            {
              final String errorMessage =
                  getErrorMessage(error.response?.data['message']);
              switch (error.response?.statusCode) {
                case 400:
                  return CustomException(
                    exceptionType: ExceptionType.badRequest,
                    statusCode: error.response?.statusCode,
                    errorResponse: error.response,
                    message: errorMessage,
                  );

                case 401:
                  Fluttertoast.showToast(msg: 'Session Expired');
                  getIt<SharedPreferencesService>().logout();
                  return CustomException(
                    exceptionType: ExceptionType.unAuthorized,
                    statusCode: error.response?.statusCode,
                    errorResponse: error.response,
                    message: 'Unauthorized',
                  );
                case 403:
                  return CustomException(
                    exceptionType: ExceptionType.unAuthorized,
                    statusCode: error.response?.statusCode,
                    errorResponse: error.response,
                    message: 'Blocked',
                  );
                case 404:
                  return CustomException(
                    exceptionType: ExceptionType.notFound,
                    statusCode: error.response?.statusCode,
                    errorResponse: error.response,
                    message: 'Not Found',
                  );
                case 422:
                  return CustomException(
                    exceptionType: ExceptionType.validationError,
                    statusCode: error.response?.statusCode,
                    errorResponse: error.response,
                    message: errorMessage.trim(),
                  );
                default:
                  return CustomException(
                    exceptionType: ExceptionType.unRecognized,
                    statusCode: error.response?.statusCode,
                    message: errorMessage.trim(),
                    errorResponse: error.response,
                  );
              }
            }
          case DioExceptionType.badCertificate:
          case DioExceptionType.connectionError:
            return CustomException(
              exceptionType: ExceptionType.noInternet,
              statusCode: error.response?.statusCode,
              errorResponse: error.response,
              message: 'No Internet Connection',
            );
          default:
            return CustomException(
              exceptionType: ExceptionType.unRecognized,
              message: 'Error unrecognized',
              errorResponse: error.response,
              statusCode: error.response?.statusCode,
            );
        }
      } else {
        return CustomException(
          exceptionType: ExceptionType.unRecognized,
          message: 'Error unrecognized',
        );
      }
    } on FormatException catch (e) {
      return CustomException(
        exceptionType: ExceptionType.formatIncorrect,
        message: e.message,
      );
    } on Exception catch (_) {
      return CustomException(
        exceptionType: ExceptionType.unRecognized,
        message: 'Error unrecognized',
      );
    }
  }

  /// Factory constructor for creating a CustomException from a parsing exception.
  factory CustomException.fromParsingException() {
    return CustomException(
      exceptionType: ExceptionType.serialization,
      message: 'Failed to parse network response',
    );
  }

  static String getErrorMessage(dynamic message) {
    if (message is String) {
      return message;
    } else {
      try {
        String errors = '';
        message.forEach((key, value) {
          errors = '$errors\n${value[0].toString()}';
        });
        return errors;
      } catch (e) {
        return 'Unknown Error';
      }
    }
  }

  @override
  String toString() => 'CustomException(name=$name,message=$message)';
}
