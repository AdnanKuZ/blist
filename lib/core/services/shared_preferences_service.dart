import 'dart:convert';

import 'package:blist/app/routing/route_generator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:injectable/injectable.dart';
import 'package:blist/app/routing/route_navigator.dart';
import '../../features/auth/data/models/user_model.dart';

@singleton
class SharedPreferencesService {
  SharedPreferences? _prefs;

  SharedPreferencesService() {
    _initPrefs();
  }

  Future<void> _initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  static const String _userKey = 'user';
  static const String _isFirstTimeKey = 'is_first_time';

  bool get isLoggedIn => getUser() != null;

  Future<void> setIsFirstTime(bool value) async {
    await _prefs?.setBool(_isFirstTimeKey, value);
  }

  bool getIsFirstTime() => _prefs?.getBool(_isFirstTimeKey) ?? true;

  Future<void> setUser(UserModel user) async {
    final userJson = json.encode(user.toJson());
    await _prefs?.setString(_userKey, userJson);
  }

  UserModel? getUser() {
    final userString = _prefs?.getString(_userKey);
    return userString != null
        ? UserModel.fromJson(json.decode(userString))
        : null;
  }

  String? getAccessToken() {
    return getUser()?.token ?? '';
  }

  Future<void> logout() async {
    try {
      await _prefs?.remove(_userKey);
      navigatorKey.currentState!.popAllAndPush(
        Routes.login,
      );
    } catch (e) {
      print(e);
    }
  }
}
