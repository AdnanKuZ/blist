import 'package:blist/features/todos/data/models/todo_model.dart';
import 'package:blist/features/todos/data/todos_datasource.dart';
import 'package:injectable/injectable.dart';

import '../../../../../core/network/repository.dart';

@lazySingleton
class TodosRepository extends BaseRepository {
  TodosRepository(this._todosDataSource);

  final TodosDataSource _todosDataSource;

  Future addTodoDataSource({
    required String todo,
    required bool isCompleted,
    required String userId,
  }) async =>
      await repository(
        () async => await _todosDataSource.addTodoDataSource(
          {
            'todo': todo,
            'completed': isCompleted,
            'userId': userId,
          },
        ),
      );
  Future fetchTodosDataSource({
    Map<String, dynamic>? queries,
  }) async =>
      await repository(
        () async => await _todosDataSource.fetchTodosDataSource(
          queries ?? {},
        ),
        modelParser: TodoModel.fromJson,
        getModelList: true,
        dataKey: 'todos',
      );
  Future fetchSingleTodoDataSource({
    required String todoId,
  }) async =>
      await repository(
        () async => await _todosDataSource.fetchSingleTodoDataSource(
          todoId,
        ),
        modelParser: TodoModel.fromJson,
      );

  Future updateTodoDataSource({
    required String todoId,
    required String todo,
    required bool isCompleted,
  }) async =>
      await repository(
        () async => await _todosDataSource.updateTodoDataSource(
          todoId,
          {
            'todo': todo,
            'completed': isCompleted,
          },
        ),
      );
  Future deleteTodoDataSource({
    required String todoId,
  }) async =>
      await repository(
        () async => await _todosDataSource.deleteTodoDataSource(
          todoId,
        ),
      );
}
