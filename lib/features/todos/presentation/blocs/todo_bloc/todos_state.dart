import 'package:blist/core/network/dio/exceptions.dart';

abstract class TodosState {
  const TodosState();
}

class InitialTodosState extends TodosState {
  const InitialTodosState();
}

class LoadingState extends TodosState {
  const LoadingState();
}


class SingleTodoLoadingState extends TodosState {
  final String todoId;
  const SingleTodoLoadingState(this.todoId);
}

class TodosSuccessState extends TodosState {
  const TodosSuccessState();
}

class ErrorState extends TodosState {
  final String errorMessage;
  final ExceptionType exceptionType;
  const ErrorState(this.errorMessage, this.exceptionType);
}

class NoInternetState extends TodosState {
  const NoInternetState();
}
