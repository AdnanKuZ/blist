import 'package:blist/core/network/dio/exceptions.dart';
import 'package:blist/features/todos/data/todos_repository.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class FetchTodosBloc extends Bloc<FetchTodosEvent, FetchTodosState> {
  final TodosRepository _todosRepository;
  FetchTodosBloc(this._todosRepository)
      : super(const InitialFetchTodosState()) {
    on<ClickFetchTodosEvent>(_onClickFetchTodosEvent);
  }

  Future<void> _onClickFetchTodosEvent(
      ClickFetchTodosEvent event, Emitter<FetchTodosState> emit) async {
    try {
      emit(const LoadingState());
      final todos = await _todosRepository.fetchTodosDataSource();
      emit(GotTodosSuccessState(todos));
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(const ErrorState(
          'An unexpected error occurred', ExceptionType.unRecognized));
    }
  }
}
