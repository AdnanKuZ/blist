import 'package:blist/app/injection/injection.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/app/widgets/blist_appbar.dart';
import 'package:blist/app/widgets/blist_drawer.dart';
import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:blist/core/services/shared_preferences_service.dart';
import 'package:blist/core/widgets/loading.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_event.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_state.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_event.dart';
import 'package:blist/features/todos/presentation/widgets/add_todo_dialog.dart';
import 'package:blist/features/todos/presentation/widgets/todo_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TodosPage extends StatefulWidget {
  const TodosPage({super.key});

  @override
  State<TodosPage> createState() => _TodosPageState();
}

class _TodosPageState extends State<TodosPage> {
  @override
  void initState() {
    super.initState();
    context.read<FetchTodosBloc>().add(ClickFetchTodosEvent());
  }

  @override
  Widget build(BuildContext context) {
    final storage = getIt<SharedPreferencesService>();

    return Scaffold(
      backgroundColor: AppColors.primary,
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.darkBlue,
        onPressed: () {
          showAddTodoDialog(context);
        },
        child: const Icon(
          CupertinoIcons.add,
          color: Colors.white,
          size: 28,
        ),
      ),
      appBar: BlistAppBar(
        isHome: true,
        title: Text(storage.getUser()?.username ?? 'BList'),
      ),
      drawer: const BlistDrawer(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: BlocBuilder<FetchTodosBloc, FetchTodosState>(
            builder: (context, state) {
          if (state is GotTodosSuccessState) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                26.v(),
                Text(
                  'To do Tasks',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                20.v(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Today\'s Tasks',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18),
                    ),
                    TextButton(
                      child: const Text(
                        "See All",
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
                10.v(),
                Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async => context
                        .read<FetchTodosBloc>()
                        .add(ClickFetchTodosEvent()),
                    child: ListView.separated(
                      itemCount: state.todos.length,
                      separatorBuilder: (context, index) => 10.v(),
                      itemBuilder: (context, index) => TodoTile(
                        todo: state.todos[index],
                        sideColor: AppColors.tileColors[index % 5],
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else if (state is LoadingState) {
            //A shimmer effect could be placed for better user experience
            return const Center(
              child: LoadingOverlay(),
            );
          }
          return Container();
        }),
      ),
    );
  }

  void showAddTodoDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (builderContext) {
        return AddTodoDialog(
          onAdd: (String todo) {
            context.read<TodosBloc>().add(
                  AddTodoEvent(
                    todo: todo,
                    isCompleted: false,
                    userId: getIt<SharedPreferencesService>()
                            .getUser()
                            ?.id
                            .toString() ??
                        '1',
                  ),
                );
            Navigator.pop(context);
          },
        );
      },
    );
  }
}
