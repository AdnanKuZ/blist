import 'package:flutter/material.dart';

class TodoTextField extends StatelessWidget {
  const TodoTextField(this.controller, {super.key});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: const InputDecoration(hintText: 'Enter todo here'),
      validator: (value) =>
          value!.length < 6 ? 'Todo should be at least 6 characters' : null,
    );
  }
}
