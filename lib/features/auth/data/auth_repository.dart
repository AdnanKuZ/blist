import 'package:injectable/injectable.dart';
import 'package:blist/core/network/repository.dart';
import 'package:blist/features/auth/data/auth_datasource.dart';
import 'package:blist/features/auth/data/models/user_model.dart';

@lazySingleton
class AuthRepository extends BaseRepository {
  final AuthDataSource _authDataSource;

  AuthRepository(this._authDataSource);

  Future<UserModel> loginRepository({
    required String name,
    required String password,
  }) async =>
      await repository(
        () async => await _authDataSource.login(
          {
            'username': name,
            'password': password,
          },
        ),
        modelParser: UserModel.fromJson,
      );

}
