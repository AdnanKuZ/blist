import 'package:blist/app/routing/route_generator.dart';
import 'package:blist/app/routing/route_navigator.dart';
import 'package:blist/app/widgets/blist_appbar.dart';
import 'package:blist/app/widgets/blist_button.dart';
import 'package:blist/features/auth/presentation/widgets/name_field.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:blist/core/widgets/loading.dart';
import 'package:blist/features/auth/presentation/blocs/login/login_bloc.dart';
import 'package:blist/features/auth/presentation/blocs/login/login_event.dart';
import 'package:blist/features/auth/presentation/blocs/login/login_state.dart';
import 'package:blist/features/auth/presentation/services/login_service.dart';
import 'package:blist/features/auth/presentation/widgets/login_page/top_login_section.dart';
import 'package:blist/features/auth/presentation/widgets/password_field.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with LoginService {
  @override
  void initState() {
    super.initState();
    initFields();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const BlistAppBar(),
      body: BlocConsumer<LoginBloc, LoginState>(
        listener: _listener,
        builder: (context, state) => Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      32.v(),
                      const TopLoginSection(),
                      20.v(),
                      const Text('Name'),
                      10.v(),
                      NameField(
                        controller: nameCon,
                        node: nameNode,
                        onSubmit: (s) => passNode.requestFocus(),
                      ),
                      20.v(),
                      const Text('Password'),
                      10.v(),
                      PasswordField(
                        controller: passCon,
                        isObscur: isObscur,
                        node: passNode,
                        onSubmit: (s) => submit(),
                        obscureText: obscureText,
                      ),
                      16.v(),
                      InkWell(
                        onTap: () {},
                        child: Stack(
                          children: <Widget>[
                            const Text(
                              'Forget password',
                              style: TextStyle(
                                color: AppColors.hint,
                                fontSize: 14,
                                fontWeight: FontWeight.w100,
                              ),
                            ),
                            Positioned(
                              left: 0,
                              right: 0,
                              bottom: 0,
                              child: Container(
                                height: 1,
                                color: AppColors.hint,
                              ),
                            ),
                          ],
                        ),
                      ),
                      32.v(),
                      BlistButton.filled(
                        text: const Text('Continue'),
                        onPressed: submit,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (state is LoadingState) const LoadingOverlay()
          ],
        ),
      ),
    );
  }

  void _listener(BuildContext context, state) async {
    if (state is LoginSuccessState) {
      navigatorKey.currentState!.popAllAndPush(Routes.todos);
      context.read<FetchTodosBloc>().add(ClickFetchTodosEvent());
    } else if (state is NotVerifiedState) {
      try {
        submit();
      } catch (e) {
        debugPrint(e.toString());
      }
    }
  }

  void obscureText() => setState(() => isObscur = !isObscur);

  @override
  void dispose() {
    passCon.dispose();
    passNode.dispose();
    super.dispose();
  }

  void submit() {
    if (validate()) {
      context.read<LoginBloc>().add(
            ClickLoginEvent(
              name: nameCon.text,
              password: passCon.text,
            ),
          );
    }
  }
}
