import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:blist/features/auth/presentation/widgets/name_field.dart';

class ConfirmPasswordField extends StatelessWidget {
  const ConfirmPasswordField({
    required this.controller,
    required this.passController,
    required this.isObscur,
    this.node,
    this.onSubmit,
    super.key,
  });
  final TextEditingController controller;
  final TextEditingController passController;
  final bool isObscur;
  final FocusNode? node;
  final FieldSubmit onSubmit;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onFieldSubmitted: onSubmit,
      focusNode: node,
      decoration: const InputDecoration(
        hintText: 'Re-Enter Password',
      ),
      inputFormatters: [
        FilteringTextInputFormatter.deny(' '),
      ],
      obscureText: isObscur,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (s) {
        if (s != passController.text) {
          return 'Passwords do not match';
        }
        return null;
      },
    );
  }
}
