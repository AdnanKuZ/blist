import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:blist/app/injection/injection.dart';
import 'package:blist/core/network/dio/exceptions.dart';
import 'package:blist/core/services/shared_preferences_service.dart';
import 'package:blist/features/auth/data/auth_repository.dart';
import 'package:blist/features/auth/data/models/user_model.dart';
import 'package:blist/features/auth/presentation/blocs/login/login_event.dart';
import 'package:blist/features/auth/presentation/blocs/login/login_state.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository _authRepository;
  LoginBloc(this._authRepository) : super(const InitialLoginState()) {
    on<ClickLoginEvent>(_onLoginEvent);
  }

  Future<void> _onLoginEvent(
      ClickLoginEvent event, Emitter<LoginState> emit) async {
    try {
      emit(const LoadingState());
      final UserModel user = await _authRepository.loginRepository(
        name: event.name,
        password: event.password,
      );
      getIt<SharedPreferencesService>().setUser(user);
      emit(LoginSuccessState(user));
    } on CustomException catch (ex) {
      emit(ErrorState(ex.message, ex.exceptionType));
    } catch (e) {
      emit(
        const ErrorState(
            'An unexpected error occurred', ExceptionType.unRecognized),
      );
    }
  }
}
