abstract class LoginEvent {
  const LoginEvent();
}

class ClickLoginEvent extends LoginEvent {
  final String name;
  final String password;

  ClickLoginEvent({
    required this.name,
    required this.password,
  });
}
