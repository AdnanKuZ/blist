import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

mixin LoginService {
  late final GlobalKey<FormState> formKey;
  late final TextEditingController nameCon;
  late final TextEditingController passCon;
  late final FocusNode nameNode;
  late final FocusNode passNode;
  bool isObscur = false;

  void initFields() {
    nameCon = TextEditingController(text: kDebugMode ? 'atuny0' : null);
    passCon = TextEditingController(text: kDebugMode ? '9uQFF1Lh' : null);
    formKey = GlobalKey<FormState>();
    nameNode = FocusNode();
    passNode = FocusNode();
  }

  bool validate() {
    if (!formKey.currentState!.validate()) {
      return false;
    }
    return true;
  }
}
