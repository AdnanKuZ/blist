import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:blist/features/welcome/presentation/pages/welcome_page.dart';

import '../../../../../gen/assets.gen.dart';

class LogoSection extends StatefulWidget {
  const LogoSection({super.key});

  @override
  State<LogoSection> createState() => _LogoSectionState();
}

class _LogoSectionState extends State<LogoSection>
    with SingleTickerProviderStateMixin {
  late AnimationController _alignmentController;
  late Animation<double> _sizeAnimation;
  late Animation<AlignmentGeometry> _alignmentAnimation;
  late Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();

    _initAnimations();
  }

  @override
  void dispose() {
    _alignmentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _alignmentController,
        builder: (context, child) {
          return Opacity(
            opacity: _opacityAnimation.value,
            child: Align(
              alignment: _alignmentAnimation.value,
              child: SizedBox(
                height: 100,
                width: 100,
                child: Center(
                  child: Assets.png.appIcon.image(
                    width: _sizeAnimation.value,
                    height: _sizeAnimation.value,
                  ),
                ),
              ),
            ),
          );
        });
  }

  void _initAnimations() {
    _alignmentController = AnimationController(
      duration:
          Duration(milliseconds: (WelcomePage.backgroundDuration / 2).round()),
      vsync: this,
    );

    _sizeAnimation = Tween<double>(
      begin: 90.0,
      end: 75.0,
    ).animate(
      CurvedAnimation(
        parent: _alignmentController,
        curve: const Interval(
          .7,
          1.0,
          curve: Curves.easeInOut,
        ),
      ),
    );

    _alignmentAnimation = AlignmentTween(
      // Calculate initial alignment to start half outside the bottom left
      begin: const Alignment(0, -.3),
      end: const Alignment(0, 0),
    ).animate(
      CurvedAnimation(
        parent: _alignmentController,
        curve: const Interval(
          .7,
          1.0,
          curve: Curves.linear,
        ),
      ),
    );

    _opacityAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _alignmentController,
        curve: const Interval(
          0.1,
          .5,
          curve: Curves.easeInOut,
        ),
      ),
    );

    _alignmentController.forward();
  }
}
