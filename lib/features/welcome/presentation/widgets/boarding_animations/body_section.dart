import 'package:blist/app/routing/route_generator.dart';
import 'package:blist/app/routing/route_navigator.dart';
import 'package:blist/app/widgets/blist_button.dart';
import 'package:flutter/material.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:blist/features/welcome/presentation/pages/welcome_page.dart';

class BodySection extends StatefulWidget {
  const BodySection({super.key});

  @override
  State<BodySection> createState() => _BodySectionState();
}

class _BodySectionState extends State<BodySection>
    with SingleTickerProviderStateMixin {
  late AnimationController _opacityController;
  late Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();

    _initAnimations();
  }

  @override
  void dispose() {
    _opacityController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    return AnimatedBuilder(
        animation: _opacityController,
        builder: (context, child) {
          return Opacity(
            opacity: _opacityAnimation.value,
            child: SizedBox(
              width: size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Welcome to \nBList 👋',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                    ),
                  ),
                  85.v(),
                  SizedBox(
                    width: size.width * .9,
                    child: BlistButton.filled(
                      text: const Text('Log In'),
                      radius: 30,
                      buttonColor: AppColors.green,
                      onPressed: () {
                        navigatorKey.currentState!.pushNamed(Routes.login);
                      },
                    ),
                  ),
                  35.v(),
                ],
              ),
            ),
          );
        });
  }

  void _initAnimations() {
    _opacityController = AnimationController(
      duration:
          Duration(milliseconds: (WelcomePage.backgroundDuration / 2).round()),
      vsync: this,
    );

    _opacityAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _opacityController,
        curve: const Interval(
          0.1,
          .5,
          curve: Curves.easeInOut,
        ),
      ),
    );
    Future.delayed(
      Duration(milliseconds: (WelcomePage.backgroundDuration / 2).round()),
      () => _opacityController.forward(),
    );
  }
}
