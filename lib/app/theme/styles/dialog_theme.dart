import 'package:flutter/material.dart';

const dialogTheme = DialogTheme(
  backgroundColor: Colors.white,
  surfaceTintColor: Colors.transparent,
);
