import 'package:flutter/material.dart';

import 'colors.dart';

final inputDecorationTheme = InputDecorationTheme(
  fillColor: AppColors.textField,
  filled: true,
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
    borderSide: BorderSide.none,
  ),
  disabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
    borderSide: BorderSide.none,
  ),
  errorBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
    borderSide: const BorderSide(
      color: AppColors.red,
      width: 2,
    ),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
    borderSide: const BorderSide(
      color: AppColors.red,
      width: 2,
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
    borderSide: BorderSide.none,
  ),
  hintStyle: const TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 14,
    color: AppColors.hint,
  ),
  isDense: true,
  contentPadding: const EdgeInsets.symmetric(
    vertical: 12,
    horizontal: 18,
  ),
  prefixIconColor: Colors.white,
  suffixIconColor: Colors.white,
);
