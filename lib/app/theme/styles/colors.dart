import 'package:flutter/material.dart';

abstract class AppColors {
  static const primary = Color(0xFF1C1C1C);
  static const onPrimary = Color(0xFF1F1F1F);
  static const primaryOpaque = Color(0x8C040B1C);
  static const textField = Color(0xFF4D4F60);
  static const blue = Color(0xFF0146DB);
  static const darkBlue = Color(0xFF022D88);
  static const green = Color(0xFF00FFB6);
  static const lightGreen = Color(0xFF9FFFE1);
  static const hint = Color(0x8CFFFFFF);
  static const red = Color(0xFFEC4F3C);
  static const lightCyan = Color(0xFFB5CAFF);

  static const violet = Color(0xFF7B61FF);
  static const soup = Color(0xFF9DCC17);
  static const night = Color(0xFF4C4C4C);

  static const shadowColor = Color(0x2612112B);
  static const tileColors = [green, soup, night, violet, lightCyan];
}
