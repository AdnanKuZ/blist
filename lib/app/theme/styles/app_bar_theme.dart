import 'package:flutter/material.dart';

const appBarTheme = AppBarTheme(
  iconTheme: IconThemeData(
    color: Colors.white,
  ),
  color: Colors.transparent,
  scrolledUnderElevation: 0,
  elevation: 0,
  titleSpacing: 0,
);

final actionIconTheme = ActionIconThemeData(
  backButtonIconBuilder: (context) => const Icon(Icons.arrow_back_ios_new),
);
