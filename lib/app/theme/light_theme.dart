import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'styles/app_bar_theme.dart';
import 'styles/colors.dart';
import 'styles/dialog_theme.dart';
import 'styles/input_decoration_style.dart';
import 'styles/snackbar_theme.dart';

ThemeData lightTheme() => ThemeData(
      primaryColor: AppColors.green,
      hintColor: AppColors.red,
      canvasColor: AppColors.primary,
      colorScheme: const ColorScheme.light(
        primary: AppColors.green,
        onPrimary: AppColors.primary,
        background: AppColors.primary,
      ),
      dialogTheme: dialogTheme,
      inputDecorationTheme: inputDecorationTheme,
      snackBarTheme: snackBarTheme,
      appBarTheme: appBarTheme,
      actionIconTheme: actionIconTheme,
      textTheme: GoogleFonts.latoTextTheme().copyWith(
        titleLarge: textTheme.titleLarge,
        titleMedium: textTheme.titleMedium,
        bodyMedium: textTheme.bodyMedium,
        bodyLarge: textTheme.bodyLarge,
        titleSmall: textTheme.titleSmall,
      ),
    );

TextTheme textTheme = const TextTheme(
  titleLarge: TextStyle(
    color: Colors.white,
    fontSize: 24,
    fontWeight: FontWeight.w700,
  ),
  titleMedium: TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w700,
    fontSize: 18,
  ),
  bodyMedium: TextStyle(
    height: 1.2,
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  bodyLarge: TextStyle(
    color: Colors.white,
  ),
);
