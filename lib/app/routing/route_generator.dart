import 'package:blist/features/todos/presentation/blocs/fetch_todos/fetch_todos_bloc.dart';
import 'package:blist/features/todos/presentation/blocs/todo_bloc/todos_bloc.dart';
import 'package:blist/features/todos/presentation/pages/todos_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:blist/app/injection/injection.dart';
import 'package:blist/features/auth/presentation/blocs/login/login_bloc.dart';
import 'package:blist/features/auth/presentation/pages/login_page.dart';
import 'package:blist/features/welcome/presentation/pages/welcome_page.dart';

part 'routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    final appRoute = settings.name;

    switch (appRoute) {
      case Routes.welcome:
        return MaterialPageRoute(
          builder: (_) => const WelcomePage(),
        );
      case Routes.login:
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (BuildContext context) => getIt<LoginBloc>(),
            child: const LoginPage(),
          ),
        );
      case Routes.todos:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (BuildContext context) => getIt<TodosBloc>(),
              ),
              BlocProvider(
                create: (BuildContext context) => getIt<FetchTodosBloc>(),
              ),
            ],
            child: const TodosPage(),
          ),
        );
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) => const WelcomePage(),
    );
  }
}
