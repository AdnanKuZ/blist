
import 'package:flutter/material.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

extension RouteNavigator on NavigatorState {
  void pushNamed(String route, {Object? args}) {
    navigatorKey.currentState?.pushNamed(route, arguments: args);
  }

  void pushReplacementNamed(String route, {Object? args}) {
    navigatorKey.currentState?.pushReplacementNamed(route, arguments: args);
  }

  void popAllAndPush(String route) {
    navigatorKey.currentState?.pushNamedAndRemoveUntil(
      route,
      (Route<dynamic> r) => false,
    );
  }

  void popAll() {
    navigatorKey.currentState?.popUntil((Route<dynamic> r) => r.isFirst);
  }
}
