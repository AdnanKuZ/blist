part of 'route_generator.dart';

class Routes {
  static const String welcome = '/welcome';
  static const String login = '/login';
  static const String todos = '/todos';
}
