import 'package:flutter/material.dart';
import 'package:blist/gen/assets.gen.dart';

class BlistAppBar extends StatelessWidget implements PreferredSizeWidget {
  const BlistAppBar({
    super.key,
    this.isHome,
    this.title,
    this.leading,
    this.actions,
  });
  final Widget? title;
  final Widget? leading;
  final List<Widget>? actions;
  final bool? isHome;

  @override
  Widget build(BuildContext context) {
    Widget? leading = this.leading;
    List<Widget>? actions = this.actions;
    bool centerTitle = true;

    if (isHome ?? false) {
      centerTitle = true;
      actions = const [
        BlistBellButton(),
      ];
      leading = const BlistDrawerButton();
    } else {
      leading = const BlistBackButton();
    }

    return AppBar(
      automaticallyImplyLeading: false,
      title: title,
      centerTitle: centerTitle,
      leading: leading,
      actions: actions,
      titleTextStyle: Theme.of(context).textTheme.titleLarge,
    );
  }

  @override
  Size get preferredSize {
    const appbarSize = Size.fromHeight(56);

    return Size(
      appbarSize.width,
      appbarSize.height,
    );
  }
}

class BlistBackButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const BlistBackButton({
    this.onPressed,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        if (onPressed != null) {
          onPressed!();
        } else {
          Navigator.pop(context);
        }
      },
      icon: const Icon(Icons.arrow_back_ios_new),
      splashRadius: 24,
    );
  }
}

class BlistBellButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const BlistBellButton({
    this.onPressed,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        if (onPressed != null) {
          onPressed!();
        }
      },
      icon: Assets.svg.bell.svg(),
      splashRadius: 24,
    );
  }
}

class BlistDrawerButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const BlistDrawerButton({
    this.onPressed,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        final bool isDrawerOpen = Scaffold.of(context).isDrawerOpen;
        if (isDrawerOpen) {
          Navigator.pop(context);
        } else {
          Scaffold.of(context).openDrawer();
        }
      },
      icon: Assets.svg.drawer.svg(),
      splashRadius: 24,
    );
  }
}
