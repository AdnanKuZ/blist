import 'package:blist/app/injection/injection.dart';
import 'package:blist/core/services/shared_preferences_service.dart';
import 'package:flutter/material.dart';
import 'package:blist/app/theme/styles/colors.dart';
import 'package:blist/core/extensions/sizedbox_extension.dart';
import 'package:blist/gen/assets.gen.dart';

class BlistDrawer extends StatelessWidget {
  const BlistDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.primary,
      child: SizedBox(
        height: MediaQuery.sizeOf(context).height,
        width: MediaQuery.sizeOf(context).width * .65,
        child: SingleChildScrollView(
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  15.v(),
                  InkWell(
                    borderRadius: BorderRadius.circular(60),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
                      child: Assets.svg.drawer.svg(),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  31.v(),
                  DrawerItem(
                    title: 'Help & Support',
                    image: Assets.svg.support.svg(),
                    onPressed: () {},
                  ),
                  4.v(),
                  DrawerItem(
                    title: 'Contact Us',
                    image: Assets.svg.contactUs.svg(),
                    onPressed: () {},
                  ),
                  4.v(),
                  DrawerItem(
                    title: 'Privacy policy',
                    image: Assets.svg.privacyPolicy.svg(),
                    onPressed: () {},
                  ),
                  4.v(),
                  DrawerItem(
                    title: 'Log out',
                    image: Assets.svg.logout.svg(),
                    onPressed: () {
                      getIt<SharedPreferencesService>().logout();
                    },
                  ),
                  4.v(),
                  DrawerItem(
                    title: 'Settings',
                    image: Assets.svg.settings.svg(),
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class DrawerItem extends StatelessWidget {
  const DrawerItem({
    required this.title,
    required this.image,
    required this.onPressed,
    super.key,
  });
  final String title;
  final Widget image;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      borderRadius: BorderRadius.circular(6),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
        child: Row(
          children: [
            image,
            12.h(),
            Expanded(child: Text(title)),
          ],
        ),
      ),
    );
  }
}
