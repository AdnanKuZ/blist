import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TogglePasswordIconButton extends StatelessWidget {
  const TogglePasswordIconButton({
    required this.onPressed,
    required this.isObscured,
    super.key,
  });

  final VoidCallback onPressed;
  final bool isObscured;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(
        end: 6,
      ),
      child: IconButton(
        onPressed: onPressed,
        icon: isObscured
            ? const Icon(
                CupertinoIcons.eye_fill,
                color: Colors.white,
                size: 22,
              )
            : const Icon(
                CupertinoIcons.eye_slash_fill,
                color: Colors.white,
                size: 22,
              ),
      ),
    );
  }
}
