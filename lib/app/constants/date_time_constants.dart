abstract class DateTimeConstants {
  static const yyyyMMddSlash = 'yyyy/MM/dd';
  static const ddMMyyyyDash = 'dd-MM-yyyy';
  static const dMMMMyyyySpace = 'd MMMM yyyy';
  static const yyyyMMddDash = 'yyyy-MM-dd';
  static const dMMMMSpace = 'd MMMM';
  static const hhmma = 'hh:mm a';
  static const dateTimeFormat = '$yyyyMMddSlash|$hhmma';
}
