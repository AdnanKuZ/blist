import 'package:flutter/material.dart';

class AppShadows {
  static const BoxShadow defaultShadow = BoxShadow(
    color: Color(0x2612112B), // Shadow color
    blurRadius: 13, // Blur radius
    offset: Offset(0, 4), // Offset in x and y directions
  );
}
