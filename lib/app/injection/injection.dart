import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:blist/app/injection/injection.config.dart';

//To generate DIs annotate each service with the :@injectable annotation. Then run the build runner. Annotations like: @singleton, @lazySingleton, @factory
//injection module are used to register dependencies that otherwise can't be annotated, such as third-party packages.
GetIt getIt = GetIt.instance;

@InjectableInit(
  preferRelativeImports: true,
)
void configureDependencies() => getIt.init();
