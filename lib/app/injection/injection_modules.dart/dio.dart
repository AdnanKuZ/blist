import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:blist/app/injection/injection.dart';
import 'package:blist/core/network/dio/dio_service.dart';
import 'package:blist/core/network/dio/interceptors/logging_interceptor.dart';
import 'package:blist/core/services/shared_preferences_service.dart';

import '../../../core/network/dio/interceptors/api_interceptor.dart';

// Options for Dio HTTP client
BaseOptions options = BaseOptions(
  connectTimeout: const Duration(milliseconds: kDebugMode ? 8000 : 15000),
  receiveTimeout: const Duration(milliseconds: kDebugMode ? 8000 : 15000),
  sendTimeout: const Duration(milliseconds: kDebugMode ? 8000 : 15000),
  responseType: ResponseType.json,
);

@module
abstract class DioInjectionModule {
  @singleton
  Dio get dio => Dio(options);

  @singleton
  DioService dioService(Dio dio, ApiInterceptor apiInterceptor) => DioService(
        dioClient: dio,
        interceptors: [
          ApiInterceptor(getIt<SharedPreferencesService>()),
          if (kDebugMode) LoggingInterceptor(),
        ],
      );
}
