// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../core/network/dio/dio_service.dart' as _i6;
import '../../core/network/dio/interceptors/api_interceptor.dart' as _i5;
import '../../core/services/shared_preferences_service.dart' as _i3;
import '../../features/auth/data/auth_datasource.dart' as _i7;
import '../../features/auth/data/auth_repository.dart' as _i10;
import '../../features/auth/presentation/blocs/login/login_bloc.dart' as _i13;
import '../../features/todos/data/todos_datasource.dart' as _i8;
import '../../features/todos/data/todos_repository.dart' as _i9;
import '../../features/todos/presentation/blocs/fetch_todos/fetch_todos_bloc.dart'
    as _i12;
import '../../features/todos/presentation/blocs/todo_bloc/todos_bloc.dart'
    as _i11;
import 'injection_modules.dart/dio.dart' as _i14;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final dioInjectionModule = _$DioInjectionModule();
    gh.singleton<_i3.SharedPreferencesService>(
        () => _i3.SharedPreferencesService());
    gh.singleton<_i4.Dio>(() => dioInjectionModule.dio);
    gh.singleton<_i5.ApiInterceptor>(
        () => _i5.ApiInterceptor(gh<_i3.SharedPreferencesService>()));
    gh.singleton<_i6.DioService>(() => dioInjectionModule.dioService(
          gh<_i4.Dio>(),
          gh<_i5.ApiInterceptor>(),
        ));
    gh.lazySingleton<_i7.AuthDataSource>(
        () => _i7.AuthDataSource(gh<_i6.DioService>()));
    gh.lazySingleton<_i8.TodosDataSource>(
        () => _i8.TodosDataSource(gh<_i6.DioService>()));
    gh.lazySingleton<_i9.TodosRepository>(
        () => _i9.TodosRepository(gh<_i8.TodosDataSource>()));
    gh.lazySingleton<_i10.AuthRepository>(
        () => _i10.AuthRepository(gh<_i7.AuthDataSource>()));
    gh.factory<_i11.TodosBloc>(() => _i11.TodosBloc(gh<_i9.TodosRepository>()));
    gh.factory<_i12.FetchTodosBloc>(
        () => _i12.FetchTodosBloc(gh<_i9.TodosRepository>()));
    gh.factory<_i13.LoginBloc>(() => _i13.LoginBloc(gh<_i10.AuthRepository>()));
    return this;
  }
}

class _$DioInjectionModule extends _i14.DioInjectionModule {}
