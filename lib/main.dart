import 'package:flutter/material.dart';
import 'package:blist/app/app.dart';

void main() async {
  await MyApp.init();
  runApp(
    const MyApp(),
  );
}
