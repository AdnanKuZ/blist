/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsAudioGen {
  const $AssetsAudioGen();

  /// File path: assets/audio/sound.mp3
  String get sound => 'assets/audio/sound.mp3';

  /// List of all assets
  List<String> get values => [sound];
}

class $AssetsJsonGen {
  const $AssetsJsonGen();

  /// File path: assets/json/error.json
  String get error => 'assets/json/error.json';

  /// File path: assets/json/loading.json
  String get loading => 'assets/json/loading.json';

  /// File path: assets/json/no_internet.json
  String get noInternet => 'assets/json/no_internet.json';

  /// File path: assets/json/notification.json
  String get notification => 'assets/json/notification.json';

  /// File path: assets/json/under_development.json
  String get underDevelopment => 'assets/json/under_development.json';

  /// List of all assets
  List<String> get values =>
      [error, loading, noInternet, notification, underDevelopment];
}

class $AssetsPngGen {
  const $AssetsPngGen();

  /// File path: assets/png/app_icon.png
  AssetGenImage get appIcon => const AssetGenImage('assets/png/app_icon.png');

  $AssetsPngWelcomeGen get welcome => const $AssetsPngWelcomeGen();

  /// List of all assets
  List<AssetGenImage> get values => [appIcon];
}

class $AssetsSvgGen {
  const $AssetsSvgGen();

  /// File path: assets/svg/bell.svg
  SvgGenImage get bell => const SvgGenImage('assets/svg/bell.svg');

  /// File path: assets/svg/contact_us.svg
  SvgGenImage get contactUs => const SvgGenImage('assets/svg/contact_us.svg');

  /// File path: assets/svg/drawer.svg
  SvgGenImage get drawer => const SvgGenImage('assets/svg/drawer.svg');

  /// File path: assets/svg/exclamation.svg
  SvgGenImage get exclamation =>
      const SvgGenImage('assets/svg/exclamation.svg');

  /// File path: assets/svg/logout.svg
  SvgGenImage get logout => const SvgGenImage('assets/svg/logout.svg');

  /// File path: assets/svg/privacy_policy.svg
  SvgGenImage get privacyPolicy =>
      const SvgGenImage('assets/svg/privacy_policy.svg');

  /// File path: assets/svg/settings.svg
  SvgGenImage get settings => const SvgGenImage('assets/svg/settings.svg');

  /// File path: assets/svg/support.svg
  SvgGenImage get support => const SvgGenImage('assets/svg/support.svg');

  /// File path: assets/svg/upload.svg
  SvgGenImage get upload => const SvgGenImage('assets/svg/upload.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        bell,
        contactUs,
        drawer,
        exclamation,
        logout,
        privacyPolicy,
        settings,
        support,
        upload
      ];
}

class $AssetsPngWelcomeGen {
  const $AssetsPngWelcomeGen();

  /// File path: assets/png/welcome/splash_background.png
  AssetGenImage get splashBackground =>
      const AssetGenImage('assets/png/welcome/splash_background.png');

  /// File path: assets/png/welcome/welcome_colors_background.png
  AssetGenImage get welcomeColorsBackground =>
      const AssetGenImage('assets/png/welcome/welcome_colors_background.png');

  /// List of all assets
  List<AssetGenImage> get values => [splashBackground, welcomeColorsBackground];
}

class Assets {
  Assets._();

  static const $AssetsAudioGen audio = $AssetsAudioGen();
  static const $AssetsJsonGen json = $AssetsJsonGen();
  static const $AssetsPngGen png = $AssetsPngGen();
  static const $AssetsSvgGen svg = $AssetsSvgGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
